﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsService1
{
    public interface ISubscriber
    {
        void Subscribe(string channel, Action<string, string> handler);
        void Unsubscribe(string channel, Action<string, string> handler = null);
    }
}
