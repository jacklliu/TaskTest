﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace WebApplication1
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //aa();
            //bb();
        }

        void aa()
        {
            int aa = 0;
            ThreadPool.QueueUserWorkItem((a) =>
            {
                while (true)
                {
                    aa = aa + 1;
                    Thread.Sleep(5000);
                }
            });
        }
        void bb()
        {
            int bb = 0;
            ThreadPool.QueueUserWorkItem((a) =>
            {
                while (true)
                {
                    bb = bb + 1;
                    Thread.Sleep(5000);
                }
            });
        }
    }
}
