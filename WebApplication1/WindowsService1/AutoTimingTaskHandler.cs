﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsService1
{
    public class AutoTimingTaskHandler
    {
        //业务类
        private readonly Repository _repository;
        public AutoTimingTaskHandler()
        {
            _repository = Repository.CreateInstance();
        }
        log4net.ILog Logger => log4net.LogManager.GetLogger(GetType());

        #region 清除日志文件
        public void ClearLogFile()
        {
            try
            {
                int flag = int.Parse(_repository.GetSysConfigByCode(SYS_CONFIG.ISClearLogFile));
                if (flag == 0)
                    return;
                Logger.Debug($"service ClearLogFile");
                string path = "..\\Services";// _repository.GetSysConfigByCode(SYS_CONFIG.ServicePath);
                int month = int.Parse(_repository.GetSysConfigByCode(SYS_CONFIG.LogOutTime));
                DateTime eTime = DateTime.Now.AddMonths(-month);
                List<LogFileInfo> fileList = GetFileList(new List<LogFileInfo>(), path, eTime);
                foreach (var item in fileList)
                {
                    File.Delete(item.Path);
                }
                //记录手动删除日志文件  执行人、删除时间、删除哪个时间段的日志
                _repository.AddSystemLog("清除 " + eTime.ToString("yyyy-MM-dd") + " 之前的 日志文件");
            }
            catch (Exception ex)
            {
                Logger.Error("ClearLogFile", ex);
            }
        }

        private List<LogFileInfo> GetFileList(List<LogFileInfo> fileList, string path, DateTime eTime)
        {
            try
            {
                string[] dics = Directory.GetDirectories(path);
                foreach (var item in dics)
                {
                    if (Directory.GetDirectories(item).Length != 0)
                    {
                        GetFileList(fileList, item, eTime);
                    }
                    DateTime nowTime = DateTime.Now;
                    DirectoryInfo root = new DirectoryInfo(item);
                    FileInfo[] fileDics = root.GetFiles();

                    if (root.Name.ToLower() == "log" || root.FullName.ToLower().Contains("\\log\\"))
                    {
                        foreach (FileInfo file in fileDics)//遍历文件夹
                        {
                            if (file.CreationTime < eTime)
                            {
                                fileList.Add(new LogFileInfo() { FileName = file.Name, CreateDate = file.CreationTime.ToString("yyyy-MM-dd HH:mm:sss"), Path = file.FullName });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetFileList", ex);
            }

            return fileList;
        }
        #endregion

        #region 备份数据
        public void BakData()
        {
            try
            {
                int flag = int.Parse(_repository.GetSysConfigByCode(SYS_CONFIG.IsBackUpBusinessData));
                if (flag == 0)
                    return;
                Logger.Debug($"service BakData");
                string bakFilePath = _repository.GetSysConfigByCode(SYS_CONFIG.BackUpBusinessDataPath);
                string bakDataBaseName = _repository.GetSysConfigByCode(SYS_CONFIG.BusinessDataBackUpDataBase);
                int month = int.Parse(_repository.GetSysConfigByCode(SYS_CONFIG.LogOutTime));
                DateTime eTime = DateTime.Now.AddMonths(-month);
                if (!Directory.Exists(bakFilePath))
                {
                    Directory.CreateDirectory(bakFilePath);
                }

                Dictionary<string, object> dict = new Dictionary<string, object>();
                dict.Add("bakFilePath", bakFilePath);
                dict.Add("eTime", eTime.ToString("yyyy-MM-dd"));
                dict.Add("bakDataBaseName", bakDataBaseName);
                _repository.BakData(dict);
                //记录操作日志
                _repository.AddSystemLog("备份业务历史数据，路径：" + bakFilePath + "，日期：" + eTime.ToString("yyyy-MM-dd HH:mm:sss") + "，数据库名称：" + bakDataBaseName);
            }
            catch (Exception ex)
            {
                Logger.Error("BackUpData", ex);
            }
        }
        #endregion

        #region 清除日志数据
        public void ClearLogData()
        {
            try
            {
                int flag = int.Parse(_repository.GetSysConfigByCode(SYS_CONFIG.ISClearNonBusinessData));
                if (flag == 0)
                    return;
                Logger.Debug($"service ClearLogData");
                string names = _repository.GetSysConfigByCode(SYS_CONFIG.NonBusinessDataTableNames);
                int month = int.Parse(_repository.GetSysConfigByCode(SYS_CONFIG.LogOutTime));
                DateTime eTime = DateTime.Now.AddMonths(-month);
                Dictionary<string, object> dict = new Dictionary<string, object>();
                dict.Add("tableNames", names);
                dict.Add("eTime", eTime);
                dict.Add("type", "LogData");
                //记录手动删除日志文件  执行人、删除时间、删除哪个时间段的日志
                _repository.AddSystemLog("清除 日期：" + eTime.ToString("yyyy-MM-dd HH:mm:sss") + " 之前的 日志数据");
                _repository.ClearLogData(dict);
            }
            catch (Exception ex)
            {
                Logger.Error("ClearLogData", ex);
            }

        }
        #endregion

    }

    public static class SubscribeMessageType
    {
        public const string ClearLogFile = "ClearLogFile";
        public const string BakData = "BakData";
        public const string ClearLogData = "ClearLogData";
    }
    public static class SYS_CONFIG
    {
        /// <summary>
        /// 服务路径
        /// </summary>
        public const string ServicePath = "ServicePath";
        /// <summary>
        /// 日志存留时间
        /// </summary>
        public const string LogOutTime = "LogOutTime";
        /// <summary>
        /// 非业务数据存留时间
        /// </summary>
        public const string NonBusinessDataOutTime = "NonBusinessDataOutTime";
        /// <summary>
        /// 是否开启自动删除非业务数据
        /// </summary>
        public const string ISClearNonBusinessData = "ISClearNonBusinessData";
        /// <summary>
        /// 是否开启自动删除日志文件
        /// </summary>
        public const string ISClearLogFile = "ISClearLogFile";
        /// <summary>
        /// 非业务数据自动清理表集合
        /// </summary>
        public const string NonBusinessDataTableNames = "NonBusinessDataTableNames";
        /// <summary>
        /// 自动备份业务数据路径
        /// </summary>
        public const string BackUpBusinessDataPath = "BackUpBusinessDataPath";
        /// <summary>
        /// 是否开启自动备份业务数据
        /// </summary>
        public const string IsBackUpBusinessData = "IsBackUpBusinessData";
        /// <summary>
        /// 自动备份业务数据存留时间
        /// </summary>
        public const string BusinessDataOutTime = "BusinessDataOutTime";
        /// <summary>
        /// 自动备份业务数据库名字
        /// </summary>
        public const string BusinessDataBackUpDataBase = "BusinessDataBackUpDataBase";
    }
    public class LogFileInfo
    {
        public string FileName { get; set; }
        public string CreateDate { get; set; }
        public string Path { get; set; }
    }
}
