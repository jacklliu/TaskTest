﻿using FastReport.Web;
using System.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            

            var actionPath = @"SchTackFile\clearlogfile\clearlogfile_exc.bat";
            TaskTriggerArg arg = new TaskTriggerArg()
            {
                TaskName = "ClearATSLog",
                TaskCreator = "admin",
                TaskDescription = "清除服务日志文件",
                Interval = "",
                StartBoundary = DateTime.Now.ToString("yyyy-MM-ddT02:01:00"),
                ActionPath = Server.MapPath("/") + actionPath + ".vbs",
                ActionArguments = Server.MapPath("/") + actionPath + " " + Server.MapPath("/") + @"..\Service" + " 0 " + "*2*",
                TaskTriggerType = TaskTriggerType.TASK_TRIGGER_WEEKLY
            };
            SchTaskExt.CreateTaskScheduler(arg);

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public void Add()
        {
            QuartzManager.AddJob<TestJob>("tjobB", "*/6 * * * * ? *");
            //await Quartz.Start<TestJob>("tj", "*/3 * * * * ? *", null);
        }
        public void Delete()
        {
            //SchTaskExt.DeleteTask("ClearATSLog");
            QuartzManager.DeleteJob("tjobB");
            //await Quartz.End("tj");
        }

        //public void AddC()
        //{
        //    QuartzManager.AddJob<TestJobC>("tjobC", "*/5 * * * * ? *");
        //    //await Quartz.Start<TestJob>("tj", "*/3 * * * * ? *", null);
        //}
        //public void DeleteC()
        //{
        //    QuartzManager.DeleteJob("tjobC");
        //    //await Quartz.End("tj");
        //}
    }
}