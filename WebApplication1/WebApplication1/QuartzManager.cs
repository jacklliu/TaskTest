﻿using Quartz;
using Quartz.Impl;
using Quartz.Impl.Triggers;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace WebApplication1
{
    /// <summary>
    /// iis会定期回收，也会清除job;iis中断也会清除job
    /// </summary>
    public class QuartzManager
    {
        private static IScheduler sched = null;

        static QuartzManager()
        {
            ISchedulerFactory sf = new StdSchedulerFactory();
            sched = sf.GetScheduler();
            sched.Start();
        }
        /// <summary>
        /// 添加Job 并且以定点的形式运行
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="JobName"></param>
        /// <param name="CronTime"></param>
        /// <param name="jobDataMap"></param>
        /// <returns></returns>
        public static void AddJob<T>(string JobName, string CronTime, string jobData) where T : IJob
        {
            IJobDetail jobCheck = JobBuilder.Create<T>().WithIdentity(JobName, JobName + "_Group").UsingJobData("jobData", jobData).Build();
            CronScheduleBuilder csb = CronScheduleBuilder.CronSchedule(CronTime);
            csb.WithMisfireHandlingInstructionFireAndProceed();

            ITrigger trigger = TriggerBuilder.Create()
                                .WithIdentity(JobName + "_Trigger", JobName + "_TriggerGroup")
                                //正常执行下一个周期的任务
                                .WithCronSchedule(CronTime, a => a.WithMisfireHandlingInstructionFireAndProceed())
                                .ForJob(JobName, JobName + "_Group")
                                .Build();
            sched.ScheduleJob(jobCheck, trigger);
        }

        /// <summary>
        /// 添加Job 并且以定点的形式运行
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="JobName"></param>
        /// <param name="CronTime"></param>
        /// <returns></returns>
        public static void AddJob<T>(string JobName, string CronTime) where T : IJob
        {
            AddJob<T>(JobName, CronTime, null);
        }
        /// <summary>
        /// 删除Job
        /// 删除功能Quartz提供有很多,以后可扩充
        /// </summary>
        /// <param name="JobName"></param>
        public static void DeleteJob(string JobName)
        {
            JobKey jk = new JobKey(JobName, JobName + "_Group");
            sched.DeleteJob(jk);
        }
    }
}