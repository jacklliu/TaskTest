﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsService1
{
    class RedisSubscriber : ISubscriber
    {
        /// <summary>
        /// 登记条目。封装：主题、订阅者委托、Redis委托
        /// </summary>
        class RegisterItem
        {
            /// <summary>
            /// 订阅主题
            /// 作用：1）取消指定主题的全部订阅时，需要删除指定主题的委托登记；2）取消订阅时，需要按主题过滤Handler。
            /// </summary>
            public string Channel;
            
            /// <summary>
            /// 订阅者的委托
            /// 作用：取消订阅时，根据此委托找到对应的Redis委托，然后调用Redis的取消订阅
            /// </summary>
            public Action<string, string> SubscriberHandler;

            /// <summary>
            /// Redis的委托
            /// 作用：对外暴露的订阅委托是Action<string, string>，与Redis的Action<RedisChannel, RedisValue>不同，需要根据订阅者的Handler找到实际的Redis的，从而取消订阅。
            /// </summary>
            public Action<RedisChannel, RedisValue> ResisHandler;
        }

        log4net.ILog Logger => log4net.LogManager.GetLogger(GetType());

        //  登记的条目
        readonly List<RegisterItem> _register = new List<RegisterItem>();

        public StackExchange.Redis.ISubscriber Subscriber { get; set; }

        public void Subscribe(string channel, Action<string, string> handler)
        {
            var item = new RegisterItem
            {
                Channel = channel,
                SubscriberHandler = handler,
                ResisHandler = (redisChannel, value) => handler(redisChannel, value)
            };
            _register.Add(item);
            Subscriber.Subscribe(channel, item.ResisHandler);
            Logger.Debug($"subscribe, channel:{channel}");
        }

        /// <summary>
        /// 取消订阅
        /// </summary>
        /// <param name="channel">取消订阅的主题</param>
        /// <param name="handler">要取消订阅的主题。 为null时，取消主题下所有委托。
        /// 注意，此处与.NET event不同处：如果同一handler多次注册Subscribe，调用取消订阅方法Unsubscribe，将取消订阅handler的所有订阅。
        /// </param>
        public void Unsubscribe(string channel, Action<string, string> handler = null)
        {
            //  handler空，表示取消订阅所有
            if (handler == null)
            {
                Subscriber.Unsubscribe(channel);

                var count = _register.RemoveAll(i => i.Channel == channel);
                Logger.Info($"unsubscribed {channel} channel all {count} handlers.");
            }
            else
            {
                //  找到指定主题下所有委托，然后逐个取消订阅
                var items = _register.Where(i => i.Channel == channel && i.SubscriberHandler == handler);
                foreach (var item in items)
                {
                    Subscriber.Unsubscribe(channel, item.ResisHandler);
                }
                //  删除登记条目
                var count = _register.RemoveAll(i => i.Channel == channel && i.SubscriberHandler == handler);
                Logger.Info($"unsubscribed {channel} channel {count} handlers");
            }
        }
    }
}
