﻿//using Microsoft.Win32.TaskScheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskScheduler;

namespace WebApplication1
{
    public class SchTaskExt
    {
        static ITaskDefinition task;
        static ITaskFolder folder;
        static SchTaskExt()
        {
            //new scheduler
            TaskSchedulerClass scheduler = new TaskSchedulerClass();
            //pc-name/ip,username,domain,password
            scheduler.Connect(null, null, null, null);
            //get scheduler folder
            folder = scheduler.GetFolder("\\");

            task = scheduler.NewTask(0);
        }
        /// <summary>
        /// create task
        /// </summary>
        /// <param name="creator"></param>
        /// <param name="taskName"></param>
        /// <param name="path"></param>
        /// <param name="interval"></param>
        /// <returns>state</returns>
        public static void CreateTaskScheduler(TaskTriggerArg arg)
        {
            try
            {
                if (IsExists(arg.TaskName))
                {
                    return;
                }

                task.RegistrationInfo.Author = arg.TaskCreator;//creator
                task.RegistrationInfo.Description = arg.TaskDescription;//description

                //set trigger  (IDailyTrigger ITimeTrigger)
                TaskTriggerSet(arg);

                //set action param
                IExecAction action = (IExecAction)task.Actions.Create(_TASK_ACTION_TYPE.TASK_ACTION_EXEC);
                action.Path = arg.ActionPath;
                action.Arguments = arg.ActionArguments;

                task.Settings.ExecutionTimeLimit = "PT0S"; //运行任务时间超时停止任务吗? PTOS 不开启超时
                task.Settings.DisallowStartIfOnBatteries = false;//只有在交流电源下才执行
                task.Settings.RunOnlyIfIdle = false;//仅当计算机空闲下才执行

                IRegisteredTask regTask = folder.RegisterTaskDefinition(arg.TaskName, task,
                                                                    (int)_TASK_CREATION.TASK_CREATE, null, //user
                                                                    null, // password
                                                                    _TASK_LOGON_TYPE.TASK_LOGON_INTERACTIVE_TOKEN,
                                                                    "");
                ///任务不执行，_TASK_LOGON_TYPE.TASK_LOGON_GROUP
                ///无法创建，被拒绝：user：Administrator  _TASK_LOGON_TYPE.TASK_LOGON_INTERACTIVE_TOKEN，可能需要进行gpedit.msc组策略设置，对于安装人员较繁琐
                //
                ///
                //IRunningTask runTask = regTask.Run(null);


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        static void TaskTriggerSet(TaskTriggerArg arg)
        {
            var trigger = task.Triggers.Create((_TASK_TRIGGER_TYPE2)arg.TaskTriggerType);

            trigger.Repetition.Interval = arg.Interval;
            trigger.Enabled = true;
            trigger.StartBoundary = arg.StartBoundary;
            trigger.EndBoundary = arg.EndBoundary;

            switch (arg.TaskTriggerType)
            {
                case TaskTriggerType.TASK_TRIGGER_LOGON:
                case TaskTriggerType.TASK_TRIGGER_TIME:
                    ITrigger it = trigger;
                    break;
                case TaskTriggerType.TASK_TRIGGER_DAILY:
                    IDailyTrigger idt = (IDailyTrigger)trigger;
                    idt.DaysInterval = 1;
                    break;
                case TaskTriggerType.TASK_TRIGGER_WEEKLY:
                    IWeeklyTrigger iwt = (IWeeklyTrigger)trigger;
                    iwt.DaysOfWeek = (short)DaysOfWeek.Friday;
                    iwt.WeeksInterval = 2;
                    break;
                case TaskTriggerType.TASK_TRIGGER_MONTHLY:
                    IMonthlyTrigger imt = (IMonthlyTrigger)trigger;
                    imt.RunOnLastDayOfMonth = true;
                    break;
            }
        }

        /// <summary>
        /// delete task
        /// </summary>
        /// <param name="taskName"></param>
        public static void DeleteTask(string taskName)
        {
            if (!IsExists(taskName))
            {
                return;
            }
            folder.DeleteTask(taskName, 0);
        }

        /// <summary>
        /// get all tasks
        /// </summary>
        public static IRegisteredTaskCollection GetAllTasks()
        {
            IRegisteredTaskCollection tasks_exists = folder.GetTasks(1);
            return tasks_exists;
        }
        /// <summary>
        /// check task isexists
        /// </summary>
        /// <param name="taskName"></param>
        /// <returns></returns>
        static bool IsExists(string taskName)
        {
            var isExists = false;
            IRegisteredTaskCollection tasks_exists = GetAllTasks();
            for (int i = 1; i <= tasks_exists.Count; i++)
            {
                IRegisteredTask t = tasks_exists[i];
                if (t.Name.Equals(taskName))
                {
                    isExists = true;
                    break;
                }
            }
            return isExists;
        }


    }
    public class TaskTriggerArg
    {
        public string TaskName { get; set; }                    //任务名称
        public string TaskCreator { get; set; }                 //创建者
        public string TaskDescription { get; set; }              //描述
        public string Interval { get; set; }                    //运行间隔
        public string StartBoundary { get; set; }               //开始时间
        public string EndBoundary { get; set; }                 //结束时间
        public string ActionPath { get; set; }                  //触发操作
        public string ActionArguments { get; set; }             //空格隔开的参数
        public TaskTriggerType TaskTriggerType { get; set; }    //触发类型
    }
    public enum TaskTriggerType
    {
        TASK_TRIGGER_EVENT = 0,
        TASK_TRIGGER_TIME = 1,
        TASK_TRIGGER_DAILY = 2,
        TASK_TRIGGER_WEEKLY = 3,
        TASK_TRIGGER_MONTHLY = 4,
        TASK_TRIGGER_MONTHLYDOW = 5,
        TASK_TRIGGER_IDLE = 6,
        TASK_TRIGGER_REGISTRATION = 7,
        TASK_TRIGGER_BOOT = 8,
        TASK_TRIGGER_LOGON = 9,
        TASK_TRIGGER_SESSION_STATE_CHANGE = 11,
        TASK_TRIGGER_CUSTOM_TRIGGER_01 = 12
    }

    public enum DaysOfWeek
    {
        Sunday = 0X01,
        Monday = 0X02,
        Tuesday = 0X04,
        Wednesday = 0X08,
        Thursday = 0X10,
        Friday = 0X20,
        Saturday = 0X40

    }
}