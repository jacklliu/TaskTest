﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WindowsService1
{
    //需要安装服务，写一个bat文件安装
    partial class ServiceT : ServiceBase
    {
        public ISubscriber Subscriber { get; set; }
        public string AutoTimingTaskChannel = "AutoTimingTaskChannel";
        public AutoTimingTaskHandler handler { get; set; }
        Timer ClearLogFileTimer;
        Timer BakDataTimer;
        Timer ClearLogDataTimer;

        log4net.ILog Logger => log4net.LogManager.GetLogger(GetType());

        public void Start()
        {
            Logger.Debug($"service start");

            Subscriber.Subscribe(AutoTimingTaskChannel, AutoTimingTaskSubscribe);
        }

        public void Stop()
        {
            if (ClearLogFileTimer != null)
            {
                ClearLogFileTimer.Change(Timeout.Infinite, Timeout.Infinite);
                ClearLogFileTimer.Dispose();
                ClearLogFileTimer = null;
            }
            if (BakDataTimer != null)
            {
                BakDataTimer.Change(Timeout.Infinite, Timeout.Infinite);
                BakDataTimer.Dispose();
                BakDataTimer = null;
            }
            if (ClearLogDataTimer != null)
            {
                ClearLogDataTimer.Change(Timeout.Infinite, Timeout.Infinite);
                ClearLogDataTimer.Dispose();
                ClearLogDataTimer = null;
            }
            Logger.Debug("stopped");
        }

        void ClearLogFile(object o)
        {
            //获取当前系统时间并判断是否为服务时间
            TimeSpan nowDt = DateTime.Now.TimeOfDay;
            TimeSpan workStartDT = DateTime.Parse("23:00").TimeOfDay;
            TimeSpan workEndDT = DateTime.Parse("5:00").TimeOfDay;
            if (nowDt > workStartDT || nowDt < workEndDT)
                handler.ClearLogFile();
        }
        void BakData(object o)
        {
            //获取当前系统时间并判断是否为服务时间
            TimeSpan nowDt = DateTime.Now.TimeOfDay;
            TimeSpan workStartDT = DateTime.Parse("23:00").TimeOfDay;
            TimeSpan workEndDT = DateTime.Parse("5:00").TimeOfDay;
            if (nowDt > workStartDT || nowDt < workEndDT)
                handler.BakData();
        }
        void ClearLogData(object o)
        {
            //获取当前系统时间并判断是否为服务时间
            TimeSpan nowDt = DateTime.Now.TimeOfDay;
            TimeSpan workStartDT = DateTime.Parse("23:00").TimeOfDay;
            TimeSpan workEndDT = DateTime.Parse("5:00").TimeOfDay;
            if (nowDt > workStartDT || nowDt < workEndDT)
                handler.ClearLogData();
        }


        /// <summary>
        /// 订阅处理函数
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="message"></param>
        private void AutoTimingTaskSubscribe(string channel, string message)
        {
            Logger.Debug($"订阅频道：{ channel.ToString() }消息内容：{ message}");
            if (string.IsNullOrWhiteSpace(message))
                return;
            string[] msg = message.Split('|');
            try
            {
                if (msg.Length < 2)
                    return;
                string type = msg[0];
                int flag = int.Parse(msg[1]);
                switch (type)
                {
                    case SubscribeMessageType.ClearLogFile:
                        if (flag == 0 && ClearLogFileTimer != null)
                        {
                            ClearLogFileTimer.Change(Timeout.Infinite, Timeout.Infinite);
                            ClearLogFileTimer.Dispose();
                            ClearLogFileTimer = null;
                        }
                        else if (flag == 1 && ClearLogFileTimer == null)
                        {
                            ClearLogFileTimer = new System.Threading.Timer(ClearLogFile);
                            ClearLogFileTimer.Change(2 * 60 * 1000, 6 * 60 * 60 * 1000);
                        }
                        break;
                    case SubscribeMessageType.BakData:
                        if (flag == 0 && BakDataTimer != null)
                        {
                            BakDataTimer.Change(Timeout.Infinite, Timeout.Infinite);
                            BakDataTimer.Dispose();
                            BakDataTimer = null;
                        }
                        else if (flag == 1 && BakDataTimer == null)
                        {
                            BakDataTimer = new System.Threading.Timer(BakData);
                            BakDataTimer.Change(3 * 60 * 1000, 6 * 60 * 60 * 1000);
                        }
                        break;
                    case SubscribeMessageType.ClearLogData:
                        if (flag == 0 && ClearLogDataTimer != null)
                        {
                            ClearLogDataTimer.Change(Timeout.Infinite, Timeout.Infinite);
                            ClearLogDataTimer.Dispose();
                            ClearLogDataTimer = null;
                        }
                        else if (flag == 1 && ClearLogDataTimer == null)
                        {
                            ClearLogDataTimer = new System.Threading.Timer(ClearLogData);
                            ClearLogDataTimer.Change(4 * 60 * 1000, 6 * 60 * 60 * 1000);
                        }

                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Debug(ex.Message);
            }
        }


    }
}
